import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

enum Menus { reset }

class _HomeState extends State<Home> {
  int score = 0;
  int position = 0;
  String endImage = 'assets/images/good.jpeg';

  List<Question> questions = [
    Question(
        'Macron a 15 ans',
        'https://images.unsplash.com/photo-1629466451940-11b984f62742?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwxfDB8MXxyYW5kb218MHx8fHx8fHx8MTY0NTUwOTk3MA&ixlib=rb-1.2.1&q=80&utm_campaign=api-credit&utm_medium=referral&utm_source=unsplash_source&w=1080',
        false),
    Question(
        'La maison de Francois est peint en rose',
        'https://images.unsplash.com/photo-1570129477492-45c003edd2be?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MXwxfDB8MXxhbGx8fHx8fHx8fA&ixlib=rb-1.2.1&q=80&w=1080&utm_source=unsplash_source&utm_medium=referral&utm_campaign=api-credit',
        false),
    Question(
        'François est genial',
        'https://images.unsplash.com/photo-1493979969019-a78fdb263931?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwxfDB8MXxhbGx8fHx8fHx8fHwxNjE1Njc3NjEw&ixlib=rb-1.2.1&q=80&w=1080&utm_source=unsplash_source&utm_medium=referral&utm_campaign=api-credit',
        true),
    Question(
        'Le Bénin est un pays pauvre',
        'https://images.unsplash.com/photo-1615150338956-542fc878f7e4?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwxfDB8MXxyYW5kb218MHx8fHx8fHx8MTY0NTUwOTg3OQ&ixlib=rb-1.2.1&q=80&utm_campaign=api-credit&utm_medium=referral&utm_source=unsplash_source&w=1080',
        false),
    Question(
        'Poutine à envahit l\'europe en une nuit!',
        'https://images.unsplash.com/photo-1554844344-c34ea04258c4?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwxfDB8MXxyYW5kb218MHx8fHx8fHx8MTY0NTgwOTEyNQ&ixlib=rb-1.2.1&q=80&utm_campaign=api-credit&utm_medium=referral&utm_source=unsplash_source&w=1080',
        false),
  ];

  void checkChoice(bool choice, BuildContext ctx) {
    if (choice == questions[position].response) {
      score++;
      const snackbar = SnackBar(
          content: Text('Bonne réponse!'),
          duration: Duration(milliseconds: 500),
          backgroundColor: Colors.green);
      Scaffold.of(ctx).showSnackBar(snackbar);
    } else {
      const snackbar = SnackBar(
          content: Text('Mauvaise réponse!'),
          duration: Duration(milliseconds: 500),
          backgroundColor: Colors.red);
      Scaffold.of(ctx).showSnackBar(snackbar);
    }

    setState(() {
      if (position < questions.length - 1) {
        position = position + 1;
      } else {
        endImage = score == 0 ? 'assets/images/bad.jpeg' : 'assets/images/good.jpeg';
        showDialog(
            context: ctx,
            builder: (_) => AlertDialog(
                  title: const Text("Fin de la partie", style: TextStyle(fontSize: 34),),
                  content: SizedBox(
                    height: 150,
                    child: Column(
                      children: [
                        Text('Vous avez terminé avec un score de $score/${questions.length}', style: const TextStyle(fontSize: 24)),
                        Padding(padding: const EdgeInsets.only(bottom: 0, top: 10), child: Image.asset(endImage, height: 90,),)
                      ],
                    ),
                  ),
                  actions: <Widget>[
                    TextButton(
                      child: const Text('OK', style: TextStyle(fontSize: 18)),
                      onPressed: () {
                        resetQuiz();
                        Navigator.of(ctx).pop();
                      },
                    ),
                  ],
                ));
      }
    });
  }

  void resetQuiz() {
    setState(() {
      position = 0;
      score = 0;
      questions.shuffle();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xffE2f3f4),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            textDirection: TextDirection.ltr,
            children: const [
              Text( "My", style: TextStyle(color: Color(0xff000000),fontStyle: FontStyle.italic,fontSize: 34)),
              Text(" Awesome ", style: TextStyle(color:  Color(0x9a0404ff), fontStyle: FontStyle.italic, fontWeight: FontWeight.bold, fontSize: 34)),
              Text("Quiz", style: TextStyle(color:  Color(0xff000000), fontStyle: FontStyle.italic, fontSize: 34)),
            ],
          ),
          // elevation: 2.24,
        ),
        body: Builder(
            builder: (ctx) => Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        margin:
                            const EdgeInsets.only(left: 25, right: 25, top: 10),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Score $score/${questions.length}',
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                    color: Color(0xff0d164b),
                                    // fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20),
                              ),
                              PopupMenuButton<Menus>(
                                icon: const Icon(Icons.settings),
                                iconSize: 38,
                                onSelected: (Menus result) {
                                  setState(() {});
                                },
                                itemBuilder: (BuildContext context) =>
                                    <PopupMenuEntry<Menus>>[
                                  PopupMenuItem<Menus>(
                                    value: Menus.reset,
                                    child: const Text('Reprendre'),
                                    onTap: () => resetQuiz(),
                                  ),
                                ],
                              )
                            ])),
                    Container(
                      alignment: Alignment.center,
                      margin:
                          const EdgeInsets.only(left: 25, right: 25, top: 10),
                      child: ClipRRect(
                          borderRadius: const BorderRadius.vertical(
                              top: Radius.circular(10),
                              bottom: Radius.circular(10)),
                          child: Image.network(questions[position].imageUrl)),
                      height: 350,
                    ),
                    Container(
                      height: 100.0,
                      margin: const EdgeInsets.all(25),
                      width: double.infinity,
                      decoration: BoxDecoration(
                        border: Border.all(color: const Color(0x9a0404ff), width: 2),
                        borderRadius: const BorderRadius.vertical(
                            top: Radius.circular(10),
                            bottom: Radius.circular(10)),
                      ),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              '${position+1}. ${questions[position].question}',
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                  color: Color(0xff000000),
                                  // fontWeight: FontWeight.bold,
                                  fontStyle: FontStyle.normal,
                                  fontSize: 24),
                            ),
                          ]),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ElevatedButton(
                          style:  ButtonStyle(backgroundColor: MaterialStateProperty.all(const Color(
                              0xffff0000))),
                          onPressed: () => checkChoice(false, ctx),
                          child: const Icon(
                            Icons.clear,
                            color: Colors.white,
                            size: 40,
                          ),
                        ),
                        ElevatedButton(
                          style:  ButtonStyle(backgroundColor: MaterialStateProperty.all(const Color(
                              0xff168201))),
                          onPressed: () => checkChoice(true, ctx),
                          child: const Icon(
                            Icons.check,
                            color: Colors.white,
                            size: 40,
                          ),
                        ),
                      ],
                    )
                  ],
                )));
  }
}

class Question {
  final String question;
  final String imageUrl;
  final bool response;

  Question(this.question, this.imageUrl, this.response);
}
